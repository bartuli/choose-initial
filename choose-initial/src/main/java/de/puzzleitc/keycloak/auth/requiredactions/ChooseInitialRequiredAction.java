/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.puzzleitc.keycloak.auth.requiredactions;

import org.keycloak.authentication.RequiredActionContext;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.credential.CredentialProvider;
import org.keycloak.credential.CredentialTypeMetadata;
import org.keycloak.credential.CredentialTypeMetadataContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.core.Response;

/**
 * Wenn der User einen Authenticator der Kategorie TWO-FACTOR bereits
 * konfiguriert hat kann die required action direkt als abgeschlossen gesetzt
 * und verlassen werden, sonst wird das Formular zur Auswahl erzeugt und die
 * challenge Response an den User weitergeben.
 * 
 * @author Alexander Bartuli
 */
public class ChooseInitialRequiredAction implements RequiredActionProvider {

	public static final String PROVIDER_ID = "choose_initial_required_action";

	@Override
	public void evaluateTriggers(RequiredActionContext context) {

	}

	@Override
	public void requiredActionChallenge(RequiredActionContext context) {

		KeycloakSession session = context.getSession();
		RealmModel realm = context.getRealm();
		UserModel user = context.getUser();

		// Ein Stream Supplier für CredentialProvider der Category TWO-FACTOR
		Supplier<Stream<CredentialProvider>> twoFactorProvidersSupplier = () -> session.getKeycloakSessionFactory()
				.getProviderFactoriesStream(CredentialProvider.class)
				.map(providerFactory -> session.getProvider(CredentialProvider.class, providerFactory.getId()))
				.filter(provider -> Objects.equals(
						provider.getCredentialTypeMetadata(CredentialTypeMetadataContext.builder().build(session))
								.getCategory(),
						CredentialTypeMetadata.Category.TWO_FACTOR));

		// Ein Boolean, der true ist, sobald ein CredentialProvider der Kategorie
		// TWO-FACTOR für den User konfiguriert ist
		boolean hasTwoFactorCategoryConfigured = twoFactorProvidersSupplier.get()
				.anyMatch(provider -> session.userCredentialManager().isConfiguredFor(realm, user, provider.getType()));

		// Eine Liste aller CredentialTypeMetadata der CredentialProvider der Kategorie
		// TWO-FACTOR
		List<CredentialTypeMetadata> twoFactorProviderCredentialTypeMetadata = twoFactorProvidersSupplier.get().map(
				provider -> provider.getCredentialTypeMetadata(CredentialTypeMetadataContext.builder().build(session)))
				.collect(Collectors.toList());

		if (hasTwoFactorCategoryConfigured) {

			context.success();

		} else {

			Response challenge = context.form().setAttribute("twoFactors", twoFactorProviderCredentialTypeMetadata)
					.createForm("choose-initial.ftl");
			context.challenge(challenge);

		}
	}

	@Override
	public void processAction(RequiredActionContext context) {
		UserModel user = context.getUser();
		String choosenProviderId = (context.getHttpRequest().getDecodedFormParameters().getFirst("choosenProviderId"));

//    	user.removeRequiredAction(UserModel.RequiredAction.CONFIGURE_TOTP.name());
//    	user.removeRequiredAction("webauthn-register");

		if (choosenProviderId != null) {
			
			user.addRequiredAction(choosenProviderId);
			
		} else {
			
			Response challenge = context.form().setError("No Option Selected").createForm("choose-initial.ftl");
			context.challenge(challenge);
			return;
			
		}
		
		context.success();
	}

	@Override
	public void close() {

	}
}
