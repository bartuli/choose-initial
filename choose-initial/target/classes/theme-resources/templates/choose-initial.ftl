<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "header" || section = "show-username">
        <script type="text/javascript">
            function fillAndSubmit(choosenProviderId) {
                document.getElementById('choosenProvider').value = choosenProviderId;
                document.getElementById('kc-select-credential-form').submit();
            }
        </script>
        <#if section = "header">
            SELECT AUTHENTICATOR TO SETUP
        </#if>
    <#elseif section = "form">
        <form id="kc-select-credential-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
        
            <div class="${properties.kcSelectAuthListClass!}">
            
                <#list twoFactors as twoFactor>
                    <div class="${properties.kcSelectAuthListItemClass!}" onclick="fillAndSubmit('${twoFactor.createAction}')">

                        <div class="${properties.kcSelectAuthListItemIconClass!}">
                            <i class="${properties['${twoFactor.iconCssClass}']!authenticationSelection.iconCssClass} fa-2x"></i>
                        </div>
                        <div class="${properties.kcSelectAuthListItemBodyClass!}">
                            <div class="${properties.kcSelectAuthListItemHeadingClass!}">
                                ${msg('${twoFactor.displayName}')}
                            </div>
                            <div class="${properties.kcSelectAuthListItemDescriptionClass!}">
                                ${msg('${twoFactor.helpText}')}
                            </div>
                        </div>
                        <div class="${properties.kcSelectAuthListItemFillClass!}"></div>
                        <div class="${properties.kcSelectAuthListItemArrowClass!}">
                            <i class="${properties.kcSelectAuthListItemArrowIconClass!}"></i>
                        </div>
                    </div>
                </#list>
                
                <input type="hidden" id="choosenProvider" name="choosenProviderId" />
                
            </div>
            
        </form>
    </#if>
</@layout.registrationLayout>